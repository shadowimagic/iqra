import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import appUrls from 'src/services/apiUrl';
import contentService from 'src/services/content.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.page.html',
  styleUrls: ['./shop.page.scss'],
})
export class ShopPage implements OnInit {

  public imageBaseUrl = appUrls.baseUrl;

  public categories = [];
  public newestProducts = [];
  public products = [];

  constructor(
    private data: DataService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    // this.categories = this.data.getCategories();
    this.getNewestProducts();

    this.getProducts();

  }


  firstImage(d)
  {
    return d[0].attributes.url;
  }
  
  getNewestProducts(){

    contentService.getNewestProducts()
    .then((d:any)=>{

      this.newestProducts = d.data;
      console.log(this.newestProducts);

    })
    .catch(e=>{
      
    })

  }


  getProducts(){

    contentService.getProducts()
    .then((d:any)=>{

      this.products = d.data;
      console.log(this.products);

    })
    .catch(e=>{
      
    })

  }



  gotoDetail(product){

    this.navCtrl.navigateRoot('/item-details', { animationDirection: 'forward', state : {product : product}});

  }



}
