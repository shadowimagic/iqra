import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import authService from 'src/services/auth.service';
import { State } from 'src/services/store/mainstore.model';
import { AppStore } from 'src/services/store/mainstore.state';
import { UtilService } from '../util.service';
import * as mainstoreActions from 'src/services/store/mainstore.actions';
import { AuthenticationService } from 'src/services/auth/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  state:Observable<State>;
  user:Object;


  public loading: any;
  isLoading = false;

  email : string;
  password : string;
  passwordType : string = "password";

  errorMessage : string = "";

  isModalOpen : boolean = true;



  constructor(
    private util: UtilService,
    private navCtrl: NavController, 
    public loadingController: LoadingController,
    private store: Store<AppStore>,
    private authenticationService: AuthenticationService
    ) 
    { 
      this.state = store.select("state");
    }

  ngOnInit() {
  }


  togglePassword()
  {
    this.passwordType = this.passwordType == "password" ? "text" : "password";
  }

  async login() {


    this.loading = await this.loadingController.create({
      message: 'Connexion...'
    });

    this.loading.present();

    let code = Math.floor(1000 + Math.random() * 9000);
    
    let data = 
    {
      "identifier" : this.email,
      "password" : this.password,
    };

    authService.login(data)
    .then((d:any)=>{
 
      this.loading.dismiss();

      let user = d.user;
      // Enabling Side Menu
      this.util.setMenuState(true);
      
      // Save
      this.store.dispatch(new mainstoreActions.UpdateUser(user));

      localStorage.setItem("authToken", d.jwt);

      // Redirect
      this.authenticationService.login(user);            
  
      // this.navCtrl.navigateRoot('/tabs', { animationDirection: 'forward' });

    })
    .catch((e:any)=>{
 
      this.loading.dismiss();
      // console.log(e);
      let err = e.response.data.error.details.errors ? e.response.data.error.details.errors[0].message : e.response.data.error.message  ;
      this.errorMessage = "messages."+err;

    }) 


  }

}
