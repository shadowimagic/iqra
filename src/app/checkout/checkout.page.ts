import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from 'src/services/store/mainstore.model';
import { AppStore } from 'src/services/store/mainstore.state';
import * as mainstoreActions from 'src/services/store/mainstore.actions';
import userService from 'src/services/user.service';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {


  public loading: any;
  isLoading = false;

  state:Observable<State>;
  public basket:any[];

  public deliveryCharges : number = 10.0;

  constructor(
    private store: Store<AppStore>,
    public toastController :  ToastController,
    public loadingController: LoadingController,

  ) { 
      
      this.state = store.select("state") 
      this.state.subscribe(d=>{
        this.basket = d.basket;
      })
  }

  ngOnInit() {
  }

  async presentToast(_mess, _col) {
    const toast = await this.toastController.create({
      message: _mess,
      position:'top',
      duration: 2000,
      color: _col
    });
    toast.present();
  }

  
  firstImage(d)
  {
    return d[0].attributes.url;
  }
  
  async order()
  {

    this.loading = await this.loadingController.create({
      message: 'Commande...'
    });

    this.loading.present();

    let orderData = 
    {
      data:
      {   
        name : this.basket[0].attributes.name + " ...",
        amount : parseFloat(this.total()),
        deliveryCharges : this.deliveryCharges,
        status : "WAITING"
      }
      
    };

    console.log(orderData);

    userService.createOrder(orderData)
    .then(d=>{
  
        this.loading.dismiss();

        // @ts-ignore
        let order = d.data;
        console.log(order);

        for (let i = 0; i < this.basket.length; i++) 
        {
            const el = this.basket[i];
            let orderedProductData = 
            {
              data:
              {
                name : el.attributes.name,
                price: el.attributes.price,
                quantity: el.quantity,
                // @ts-ignore
                order: order.id,
                // @ts-ignore
                product: this.basket[i].id
              }
            };

            console.log(orderedProductData)

            // Creating ordered Product
            userService.createOrderedProduct(orderedProductData)
            .then(d=>{
          
            })
            .catch(e=>{

            })

        }

        this.presentToast("Commande passée avec succés", "success")


    })
    .catch(e=>{

      this.loading.dismiss();

      this.presentToast("Erreur lors du passage de commande", "danger")
    })
    
  }

  total()
  {
    let total = 0;
    for (let i = 0; i < this.basket.length; i++) {
      const el = this.basket[i];
      let v = el.attributes.price * el.quantity;

      total += v;
    }

    return total.toFixed(2);
  }

  globalCost()
  {
    let total = parseFloat(this.total());

    return (total + this.deliveryCharges).toFixed(2)
  }

}
