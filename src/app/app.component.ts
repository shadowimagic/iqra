import { Component, OnInit } from '@angular/core';

import { NavController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UtilService } from './util.service';
import { menuController } from '@ionic/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core'; // add this
import { Store } from '@ngrx/store';
import * as mainstoreActions from 'src/services/store/mainstore.actions';
import { AppStore } from 'src/services/store/mainstore.state';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { State } from 'src/services/store/mainstore.model';
import { AuthenticationService } from 'src/services/auth/authentication.service';
import auth from 'src/services/auth';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  // providers:[AuthenticationService]
})
export class AppComponent implements OnInit {

  state:Observable<State>;
  user:Object;

  public isMenuEnabled:boolean = true;
  public selectedIndex = 0;


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private util: UtilService,
    private router: Router,
    private storage:Storage,
    private translate: TranslateService, // add this,
    private store: Store<AppStore>,
    private authenticationService: AuthenticationService,
    private navCtrl: NavController 
  ) { 
      this.state = store.select("state");

      // this.manageRedirection();

      this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.store.dispatch(new mainstoreActions.InitState());


      // this.authenticationService.authState.subscribe(state => {
      //   if (state) {
      //     this.router.navigate(['tabs']);
      //   } else {
      //     this.router.navigate(['welcome']);
      //   }
      // });
      
    });
  }

  async ngOnInit() {
    // If using a custom driver:
    // await this.storage.defineDriver(MyCustomDriver)
    await this.storage.create();

    this.selectedIndex = 1;
    
    this.util.getMenuState().subscribe(menuState => {
      this.isMenuEnabled = menuState;
    });

    this.translate.setDefaultLang('en'); // add this
 
  }
 

  navigate(path, selectedId) {
    this.selectedIndex = selectedId;
    this.router.navigate([path]);
  }

  logout(){

    this.authenticationService.logout();
    let newState : State = {
      user:  {id:null},
      basket:[],
      orders:[],
      heroes:[],
      categories:[],
      featured:[],
      mostViewed:[]
    }

    auth.logout();

    this.store.dispatch(new mainstoreActions.SetState(newState));

  }

  close() {
    menuController.toggle();
  }
}
