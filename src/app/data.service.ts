import { Injectable } from '@angular/core';

// Category Interface
export interface ICategory {
  id: number,
  name: string,
  image: string,
}

// Product Interface
export interface IProduct {
  id: number,
  name: string,
  price: number,
  image: string,
}

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getCategories() {
    let categories = [];

    let cat1: ICategory = {
      id: 1,
      name: 'Collection été',
      image: '../../assets/categories/category-1.png'
    }
    let cat2: ICategory = {
      id: 2,
      name: 'Wax Ghana',
      image: '../../assets/categories/category-2.png'
    }
    let cat3: ICategory = {
      id: 3,
      name: 'Populaire',
      image: '../../assets/categories/category-3.png'
    }

    categories.push(cat1, cat2, cat3);

    return categories;
  }

  getFeaturedProducts() {
    let products = [];

    let prod1: IProduct = {
      id: 1,
      name: 'Womens Robe',
      price: 55,
      image: '../../assets/robe-1.jpg'
    }
    let prod2: IProduct = {
      id: 2,
      name: 'Womens Robe',
      price: 34,
      image: '../../assets/robe-2.jpg'
    }
    let prod3: IProduct = {
      id: 1,
      name: 'Womens Robe',
      price: 40,
      image: '../../assets/robe-3.jpg'
    }

    products.push(prod1, prod2, prod3);

    return products;
  }


  
  getProducts() {
    let products =
     [
      {
        id: 1,
        name: 'Womens Robe',
        price: 55,
        image: '../../assets/robe-1.jpg'
      },
      {
        id: 2,
        name: 'Womens Robe',
        price: 34,
        image: '../../assets/robe-2.jpg'
      },
      {
        id: 1,
        name: 'Womens Robe',
        price: 40,
        image: '../../assets/robe-3.jpg'
      },
      {
        id: 1,
        name: 'Womens Robe',
        price: 55,
        image: '../../assets/robe-1.jpg'
      },
      {
        id: 2,
        name: 'Womens Robe',
        price: 34,
        image: '../../assets/robe-2.jpg'
      },
      {
        id: 1,
        name: 'Womens Robe',
        price: 40,
        image: '../../assets/robe-3.jpg'
      },
      {
        id: 1,
        name: 'Womens Robe',
        price: 55,
        image: '../../assets/robe-4.jpg'
      },
      {
        id: 2,
        name: 'Womens Robe',
        price: 34,
        image: '../../assets/robe-2.jpg'
      },
      {
        id: 1,
        name: 'Womens Robe',
        price: 40,
        image: '../../assets/robe-3.jpg'
      },

  ]
    return products;
  }



    
  getRecentsProducts() {
    let products =
     [
      {
        id: 1,
        name: 'Womens Robe',
        price: 40,
        image: '../../assets/robe-3.jpg'
      },

  ]
    return products;
  }

  
  getCreationsProducts() {
    let products =
     [
      {
        id: 1,
        name: 'Womens Robe',
        price: 40,
        color: "tertiary",
        status: "Commandé",
        image: '../../assets/robe-3.jpg'
      },
      {
        id: 1,
        name: 'Womens Robe',
        price: 55,
        color: "success",
        status: "Prêt",
        image: '../../assets/robe-4.jpg'
      },
      {
        id: 2,
        name: 'Womens Robe',
        price: 34,
        color: "warning",
        status: "Annulé",
        image: '../../assets/robe-2.jpg'
      },
      {
        id: 1,
        name: 'Womens Robe',
        price: 40,
        color: "medium",
        status: "Livré",
        image: '../../assets/robe-3.jpg'
      },

  ]
    return products;
  }


  getBestSellProducts() {
    let products = [];

    let prod1: IProduct = {
      id: 1,
      name: 'Womens Robe',
      price: 55,
      image: '../../assets/robe-4.jpg'
    }
    let prod2: IProduct = {
      id: 2,
      name: 'Womens Robe',
      price: 34,
      image: '../../assets/robe-5.jpg'
    }
    let prod3: IProduct = {
      id: 1,
      name: 'Womens Robe',
      price: 40,
      image: '../../assets/robe-2.jpg'
    }

    products.push(prod1, prod2, prod3);

    return products;
  }
}
