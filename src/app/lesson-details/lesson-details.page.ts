import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Animation, AnimationController, NavController, ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import appUrls from 'src/services/apiUrl';
import { State } from 'src/services/store/mainstore.model';
import { AppStore } from 'src/services/store/mainstore.state';
import * as mainstoreActions from 'src/services/store/mainstore.actions';

@Component({
  selector: 'app-lesson-details',
  templateUrl: './lesson-details.page.html',
  styleUrls: ['./lesson-details.page.scss'],
})
export class LessonDetailsPage implements OnInit {

  state:Observable<State>;
  public basket:any[];

  public imageBaseUrl = appUrls.baseUrl;

  selectedSize: number;
  selectedColor: number;
  activeVariation: string;

  isInBasket: boolean = false;

  product : any;
  public basketLesson : any;

  lesson : any;

  constructor(
    private animatioCntrl: AnimationController,
    public router: Router,
    private navCtrl: NavController,
    public toastController : ToastController,
    private store: Store<AppStore>


  ) { 
          
    this.state = store.select("state") 
    this.state.subscribe(d=>{
      this.basket = d.basket;
    })
  }
  

  ngOnInit() {

    this.lesson = this.router.getCurrentNavigation().extras.state.lesson; 

    console.log(this.lesson)
  }

  goBack(){
    this.navCtrl.back({ animationDirection: 'back'});
  }
  
 

  playSound(item){

    const audio = new Audio(item.sound);
    audio.play();

  }




}
