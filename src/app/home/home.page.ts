import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from '../data.service';

import SwiperCore, { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';
import contentService from 'src/services/content.service';
import apiUrl from 'src/services/apiUrl';
import { NavController } from '@ionic/angular';
import appUrls from 'src/services/apiUrl';

//import the reducer, state, and selector
//import the actions, action type constants, and action class types

//add the Observable component so we can asynchronously link to the todo list in our application state
import { Observable} from "rxjs";
import { Store } from '@ngrx/store';
import { State, Tutorial } from 'src/services/store/mainstore.model';
import * as mainstoreActions from 'src/services/store/mainstore.actions';
import { AppStore } from 'src/services/store/mainstore.state';
import lettersLessons from 'src/services/mocks/letters.lessons';

SwiperCore.use([Autoplay, Keyboard, Pagination, Scrollbar, Zoom]);

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomePage implements OnInit {

  //make an observable
  state:Observable<State>;

  public courses = [
    {id:1, name:"Lettres", icon:"../../assets/iqra-icons/theme1-icon.png",
      lessons:lettersLessons
    },
    {id:2, name:"Sons", icon:"../../assets/iqra-icons/theme2-icon.png"},
    {id:3, name:"Doublement", icon:"../../assets/iqra-icons/theme3-icon.png"},
    {id:4, name:"Allongement", icon:"../../assets/iqra-icons/theme4-icon.png"},
    {id:5, name:"L'article “AL”", icon:"../../assets/iqra-icons/theme5-icon.png"}
  ];

  public imageBaseUrl = appUrls.baseUrl;
  public user :any;
  public heroes = [];
  public categories = [];
  public featuredProducts = [];
  public mostViewedProducts = [];

  constructor( 
    private navCtrl: NavController,
    private store: Store<AppStore>
  ) { 

    this.state = store.select("state")
    // this.tutorials = this.state["tutorials"];
    this.state.subscribe(d=>{
    
    })
  }

  ngOnInit() {

     
  }

 

  gotoCourseDetails(c){

    this.navCtrl.navigateRoot('/course-details', { animationDirection: 'forward', state : {course : c}});

  }




}
