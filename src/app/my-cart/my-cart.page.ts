import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from 'src/services/store/mainstore.model';
import { AppStore } from 'src/services/store/mainstore.state';
import * as mainstoreActions from 'src/services/store/mainstore.actions';



@Component({
  selector: 'app-my-cart',
  templateUrl: './my-cart.page.html',
  styleUrls: ['./my-cart.page.scss'],
})
export class MyCartPage implements OnInit {

  state:Observable<State>;
  public basket:any[];

  constructor(
    private store: Store<AppStore>,
    public toastController :  ToastController
  ) { 
      
      this.state = store.select("state") 
      this.state.subscribe(d=>{
        this.basket = d.basket;
      })
  }

  ngOnInit() {
  }

  async presentToast(_mess, _col) {
    const toast = await this.toastController.create({
      message: _mess,
      position:'top',
      duration: 2000,
      color: _col
    });
    toast.present();
  }
  
  firstImage(d)
  {
    return d[0].attributes.url;
  }

  removeProduct(_product)
  {
    this.store.dispatch(new mainstoreActions.RemoveBasketItem(_product.id));
    this.presentToast('Produit retiré avec succés', 'danger');

    this.state.subscribe(d=>{
      this.basket = d.basket;
    })

  }

  updateProduct(_product, value:number)
  {
    let _basketItem = this.basket.find(e=>e.id == _product.id);
    let basketItem = {..._basketItem};
    basketItem.quantity += value;

    this.store.dispatch(new mainstoreActions.UpdateBasketItem(basketItem));

    if(basketItem.quantity == 0)
    {
      // console.log("REMOVED !!!")
      this.removeProduct(_product);
    }
 
    this.state.subscribe(d=>{
      this.basket = d.basket;
    })


  }



}
