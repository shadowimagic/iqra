import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { State } from 'src/services/store/mainstore.model';
import * as TmainstoreActions from 'src/services/store/mainstore.actions';
import { Store } from '@ngrx/store';
import { NavController } from '@ionic/angular';
import { AppStore } from 'src/services/store/mainstore.state';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  state:Observable<State>;
  user:Object;

  constructor(
    private navCtrl: NavController,
    private store: Store<AppStore>
  ) { 
      this.state = store.select("state"); 
    }

  ngOnInit() {

  }


}
