import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SignupActivationPage } from './signup-activation.page';

describe('SignupActivationPage', () => {
  let component: SignupActivationPage;
  let fixture: ComponentFixture<SignupActivationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupActivationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SignupActivationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
