import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupActivationPage } from './signup-activation.page';

const routes: Routes = [
  {
    path: '',
    component: SignupActivationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignupActivationPageRoutingModule {}
