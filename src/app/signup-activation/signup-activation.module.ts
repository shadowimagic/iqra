import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { SignupActivationPageRoutingModule } from './signup-activation-routing.module';

import { SignupActivationPage } from './signup-activation.page';
import { CodeInputModule } from 'angular-code-input';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignupActivationPageRoutingModule,
    TranslateModule.forChild(),
    CodeInputModule
  ],
  declarations: [SignupActivationPage]
})
export class SignupActivationPageModule {}
