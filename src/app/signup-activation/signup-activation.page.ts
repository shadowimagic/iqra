import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import axios from 'axios';
import authService from 'src/services/auth.service';
import { Observable } from 'rxjs';
import { State } from 'src/services/store/mainstore.model';
import * as TmainstoreActions from 'src/services/store/mainstore.actions';
import { Store } from '@ngrx/store';
import { AppStore } from 'src/services/store/mainstore.state';
import { AuthenticationService } from 'src/services/auth/authentication.service';


@Component({
  selector: 'app-signup-activation',
  templateUrl: './signup-activation.page.html',
  styleUrls: ['./signup-activation.page.scss'],
})
 

export class SignupActivationPage implements OnInit {


  public loading: any;
  isLoading = false;

  code : string;

  errorMessage : string = "";

  isModalOpen : boolean = true;

  state:Observable<State>;
  user:Object;

  constructor(
    private store: Store<AppStore>,
    public loadingController: LoadingController, 
    public alertCtrl : AlertController,
    private navCtrl: NavController,
    private authenticationService: AuthenticationService
    ) 
    { 
      this.state = store.select("state");
      this.state.subscribe(d=>{
        this.user = d.user;
      })
    }

 
  ngOnInit() {

  }

  // this called every time when user changed the code
  onCodeChanged(code: string) {

  }

  // this called only if user entered full code
  async onCodeCompleted(code: string) {
 
    this.loading = await this.loadingController.create({
      message: 'Validation...'
    });

    this.loading.present();
 
    let data = 
    {
      //@ts-ignore
      "id" : this.user.id,
      //@ts-ignore
      "email" : this.user.email,
      //@ts-ignore
      "activationCode" : this.user.activationCode
    };
    

    console.log(data);

    authService.findUser(data)
    .then((d:any)=>{
 
      console.log(d);
      if(d.length > 0)
      {
        let user = d[0];

        data.id = user.id;
        authService.validateAccount(data)
        .then((d:any)=>{
    
          console.log("Validated");
          this.alertCtrl.create({message:'Compte validé avec succés !'}).then(r=>r.present());

          this.authenticationService.login(user);            

          this.navCtrl.navigateRoot('/tabs', { animationDirection: 'forward' });

          this.loading.dismiss();
    
        })
        .catch((e:any)=>{
    
          this.loading.dismiss();
    
          this.alertCtrl.create({message:'Une erreur est survenue lors de la validation. Veuillez réessayer svp !'}).then(r=>r.present());

          let err = e.response.data.error.details.errors[0].message;
          this.errorMessage = "messages."+err;

        })
      }
      else
      {
        this.alertCtrl.create({message:'Code incorrect !'}).then(r=>r.present());
      }
      // this.loading.dismiss();
 
    })
    .catch((e:any)=>{
 
      console.log(e)
      
      this.alertCtrl.create({message:'Une erreur est survenue lors de la validation. Veuillez réessayer svp !'}).then(r=>r.present());
      
      this.loading.dismiss();

    })



  }






}
