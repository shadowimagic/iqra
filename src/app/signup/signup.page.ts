import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import axios from 'axios';
import { Observable } from 'rxjs';
import authService from 'src/services/auth.service';
import { State } from 'src/services/store/mainstore.model';
import { AppStore } from 'src/services/store/mainstore.state';
import * as mainstoreActions from 'src/services/store/mainstore.actions';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
 

export class SignupPage implements OnInit {


  state:Observable<State>;
  user:Object;


  public loading: any;
  isLoading = false;

  username : string;
  email : string;
  password : string;
  passwordType : string = "password";

  errorMessage : string = "";

  isModalOpen : boolean = true;

  constructor(
    public loadingController: LoadingController,
    private navCtrl: NavController, 
    private store: Store<AppStore>
    ) 
    { 
      this.state = store.select("state");
    }

 
  ngOnInit() {

  }

  togglePassword()
  {
    this.passwordType = this.passwordType == "password" ? "text" : "password";
  }

  sendMail(to_email, code)
  {
    let data = {
      "service_id": "service_zyh579p",
      "template_id": "template_0nbsy9o",
      "user_id": "SeOrhxSe_O1qO7L2d",
      "template_params" : {
        "to_email" : to_email,
        "message" : code
      }
    };

    axios.post("https://api.emailjs.com/api/v1.0/email/send", data)
    .then(d=>{
      console.log(d);
    })
    .catch(e=>{
      console.log(e);
    })


  }

  async signup(){
  
    this.loading = await this.loadingController.create({
      message: 'Creation...'
    });

    this.loading.present();

    let code = Math.floor(1000 + Math.random() * 9000);
    
    let data = 
    {
      "username" : this.username,
      "email" : this.email,
      "password" : this.password,
      "activationCode" : code.toString()
    };

   
    authService.signup(data)
    .then((d:any)=>{
 
      this.loading.dismiss();

      // this.sendMail(this.email, code);
      let user = d.user;
      user.activationCode = code;

      console.log(code);
      // Save
      this.store.dispatch(new mainstoreActions.UpdateUser(user));
      // Redirect
      this.navCtrl.navigateRoot('/signup-activation', { animationDirection: 'forward' });

    })
    .catch((e:any)=>{
 
      this.loading.dismiss();
      let err = e.response.data.error.details.errors[0].message;
      this.errorMessage = "messages."+err;

    })

  }






}
