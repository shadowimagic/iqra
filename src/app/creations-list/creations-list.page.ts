import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-creations-list',
  templateUrl: './creations-list.page.html',
  styleUrls: ['./creations-list.page.scss'],
})
export class CreationsListPage implements OnInit {
  public categories = [];
  public recentsProducts = [];
  public products = [];

  constructor(
    private data: DataService,
  ) { }

  ngOnInit() {
    this.categories = this.data.getCategories();
    this.recentsProducts = this.data.getRecentsProducts();
    this.products = this.data.getCreationsProducts();
  }

}
