import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreationsListPage } from './creations-list.page';

describe('CreationsListPage', () => {
  let component: CreationsListPage;
  let fixture: ComponentFixture<CreationsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreationsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
