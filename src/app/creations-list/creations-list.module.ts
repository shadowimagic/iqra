import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreationsListPageRoutingModule } from './creations-list-routing.module';

import { CreationsListPage } from './creations-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreationsListPageRoutingModule
  ],
  declarations: [CreationsListPage]
})
export class CreationsListPageModule {}
