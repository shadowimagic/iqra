const { i18n } = require('i18n');
const path = require('path');

const i18n = new I18n({
  locales: ['fr', 'en'],
  defaultLocale: 'fr',
  directory: path.join('./', 'locales')
});

module.exports = i18n;