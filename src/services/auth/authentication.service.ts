import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import authService from '../auth.service';
import auth from '../auth';


@Injectable()
export class AuthenticationService {

  authState = new BehaviorSubject(false);

  constructor(
    private _router: Router,
    private _storage: Storage,
    private _platform: Platform,
    public _toastController: ToastController
  ) {
    this._platform.ready().then(() => {
        this._storage.create();
        this.ifLoggedIn();
    });
  }

  ifLoggedIn() {
    
    this._storage.get('USER_INFO').then((response) => {
      if (response) {
        this.authState.next(true);
        this._router.navigate(['tabs']);
   
      }
    });
  }


  login(user) {
   
    this._storage.set('USER_INFO', user).then((response) => {
      this._router.navigate(['tabs']);
      this.authState.next(true);
      auth.login();
    });
  }

  logout() {
    this._storage.remove('USER_INFO').then(() => {
      this._router.navigate(['welcome']);
      this.authState.next(false);
    });
  }

  isAuthenticated() {
    return this.authState.value;
  }



}