import {axios, AxiosRequestConfig} from "axios";
import { api } from "./api.config";
import apiUrl from "./apiUrl";

/**
 * uploadService 
 * 
 * Only pdf, doc, docx, image/* files can be uploaded.
 * You can upload single file at once.
 */
const uploadService = {
  uploadImage,
};

const resource = "/files";

/**
 * uploadImage method
 *
 * @param {FormData} imageFormData
 * @returns
 * 
 * This is how to upload file from frontend UI
 * First of all, make an import of upload service
 *
 * ```js
 * 
 * import uploadService from "your_relative_path/upload.service";
 * 
 * // Inside your react js code, you can do like this 🔽🔽🔽
 * 
 * var imageFormData = new FormData();
 * 
 * // You can use js querySelector to get DOM node reference of your file input or use React ref
 * // Assuming you use js querySelector, you can have the script below
 * 
 * const imageFile = document.querySelector('#file');
 * 
 * // Make sure that you append your file from "file" key in your formData, otherwise your request will be rejected.
 * 
 * imageFormData.append("file", imageFile.files[0]);
 * 
 * // After, you can use upload service to upload your file
 * uploadService.uploadImage(imageFormData).then(({ link }) => {
 *   // Your code here !!
 * });
 * ```
 * 
 */
function uploadImage(imageFormData) {

  return api.uploadFiles(resource, imageFormData);

}

export const imageBaseUrl = "https://allorabbitapi.cleverapps.io/files/"


export default uploadService;