import { api } from "./api.config";

const userService = {
  createOrder,
  createOrderedProduct,
  findUser,
  validateAccount,
  logout,
 
};

const resource = "";


/**
 * createOrder method
 *
 * @param {Object} payload
 * @returns
 */
function createOrder(payload) {
  return api.postData(resource + "/orders", payload);
}

/**
 * ordered product method
 *
 * @param {Object} payload
 * @returns
 */
 function createOrderedProduct(payload) {
  return api.postData(resource + "/ordered-products", payload);
}


/**
 * FindOne method
 *
 * @param {} payload
 * @returns
 */
 function findUser(payload) {
  let filter = `?filters[email][$eq]=${payload.email}&[activationCode][$eq]=${payload.activationCode}`;
  return api.getData(resource + "/users/"+filter);
}

/**
 * validate account method
 *
 * @param {{id: number, email: string, activationCode: string }} payload
 * @returns
 */
 function validateAccount(payload) {
  // let filter = `?filter=[email][${payload.email}]&[activationCode][${payload.activationCode}]`;
  return api.putData(resource + "/users/"+payload.id, {activated : true}, {}, {});
}


/**
 * logout method
 *
 * @returns
 */
function logout() {
  return api.postData(resource + "/logout");
}
 

export default userService;
