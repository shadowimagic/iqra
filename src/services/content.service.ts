import { api } from "./api.config";

const contentService = {
 
  getHeroes,
  getCategories,
  getFeatured,
  getMostViewedProducts,
  getNewestProducts,
  getProducts,
  getCart,
 
};

const resource = "";


/**
 * --- method
 *
 * @returns
 */
 function getHeroes() {
  return api.getData(resource + "/heroes?populate=image");
}
 

/**
 * --- method
 *
 * @returns
 */
 function getCategories() {
  return api.getData(resource + "/product-categories?populate=image");
}
 

/**
 * --- method
 *
 * @returns
 */
 function getFeatured() {
  let filter = `?filters[tag][$eq]=VEDETTES`;
  return api.getData(resource + "/products"+filter+"&populate=images");
}

/**
 * --- method
 *
 * @returns
 */
 function getMostViewedProducts() {
  let filter = `?filters[tag][$notNull]`;
  return api.getData(resource + "/products"+filter+"&populate=images");
}

/**
 * --- method
 *
 * @returns
 */
 function getNewestProducts() {
  return api.getData(resource + "/products?filters\[product_categories\][name][$contains]=Nouveauté&populate=images");
}


/**
 * --- method
 *
 * @returns
 */
 function getProducts() {
  return api.getData(resource + "/products?filters\[product_categories\][name][$notContains]=Nouveauté&populate=images");
}


/**
 * --- method
 * @param {number} userId
 * @returns
 */
 function getCart(userId) {
  let filter = `?filters[user][$eq]=${userId}`;
  return api.getData(resource + "/carts"+filter+"&populate=products");
}


export default contentService;
