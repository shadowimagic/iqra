
class Auth {

  authenticated : boolean;
  
  constructor() {
    this.authenticated = false;
  }

  login = (/* cb */) => {
    this.authenticated = true;
    localStorage.setItem("isAuthenticated", "true");
    // reactLocalStorage.set("isAuthenticated", this.authenticated);
    // cb();
  };

  logout = (/* cb */) => {
    this.authenticated = false;
 
    localStorage.setItem("isAuthenticated", "false");
    // localStorage.setItem("user", JSON.stringify(bareUser));
    localStorage.removeItem("authToken");

    // reactLocalStorage.set("isAuthenticated", this.authenticated);
    // reactLocalStorage.set("user", null);
    // reactLocalStorage.remove("authToken");
    // cb();
  };

  isAuthenticated = () => {
    let auth = (localStorage.getItem("isAuthenticated")) || false;
    this.authenticated = auth === "true";
    return this.authenticated;

    // let auth = reactLocalStorage.get("isAuthenticated") || false;
    // // this.authenticated = auth;
    // this.authenticated = auth === 'true';
    // return this.authenticated;
  };
}

export default new Auth();
