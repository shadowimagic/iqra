
const baseUrl = "https://mprew-dash.herokuapp.com";
const apiUrl = baseUrl + "/api";

const appUrls = {
  baseUrl,
  apiUrl
}

export default appUrls;
