import { State, Tutorial } from "./mainstore.model";

export interface AppStore {
    state: State;
}

export const initialState : AppStore = {
    state : {
        
        user:{
            id:null,
        },
        basket : [],

        orders:[],

        heroes:[],
        categories:[],
        featured:[],
        mostViewed:[],
       
    }
    
}
 
