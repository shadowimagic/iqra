export interface State {
 
    user: Object;
    basket: any[];


    heroes: any[];
    categories: any[];
    featured: any[];
    mostViewed: any[];

    orders: any[];
    
}

export interface Tutorial {
    name: string;
     url: string;
}

