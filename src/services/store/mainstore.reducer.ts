import { Action } from '@ngrx/store'
import { State} from './mainstore.model'
import * as mainStoreActions from './mainstore.actions'
import { AppStore, initialState } from './mainstore.state'
// import { initialState } from './mainstore.state';
import * as mainstoreTypes from './mainstore.types'
 

export function reducer(state: State = initialState.state, action: mainStoreActions.Actions) {
    switch(action.type) {
     
        case mainstoreTypes.INIT_STATE:
            return state;

        case mainstoreTypes.SET_STATE:
            state = action.payload;
            return state;

        case mainstoreTypes.SET_HEROES:
            // console.log(action.payload)
            var _heroes = action.payload;
            return {...state, heroes: _heroes};

        case mainstoreTypes.SET_CATEGORIES:
            // console.log(action.payload)
            var _categories = action.payload;
            return {...state, categories: _categories};

        case mainstoreTypes.SET_FEATURED:
            // console.log(action.payload)
            var _featured = action.payload;
            return {...state, featured: _featured};
    
        case mainstoreTypes.SET_MOST_VIEWED:
            // console.log(action.payload)
            var _mostViewed = action.payload;
            return {...state, mostViewed: _mostViewed};
    
        case mainstoreTypes.SET_USER:
            // console.log(action.payload)
            var _user = action.payload;
            return {...state, user: _user};

        case mainstoreTypes.UPDATE_USER:
            // console.log(action.payload)
            var _user = {...state.user};
            _user = {..._user, ...action.payload};

            return {...state, user: _user};


        case mainstoreTypes.ADD_BASKET_ITEM:
            // console.log(action.payload)
            var _arr = [...state.basket];
            _arr.push(action.payload)
             
            return {...state, basket : _arr};

        case mainstoreTypes.REMOVE_BASKET_ITEM:

            var _arr = [...state.basket];
            var _itemIndex = _arr.findIndex(e=>e.id == action.payload);
            _arr.splice(_itemIndex, 1)

            return {...state, basket : _arr};

        case mainstoreTypes.UPDATE_BASKET_ITEM:
            var items = [...state.basket];

            var _itemIndex = items.findIndex(e=>e.id == action.payload.id);
            
            items[_itemIndex] = action.payload;
         
            return {...state, basket: items};


        case mainstoreTypes.SET_ORDERS:
            // console.log(action.payload)
            var _categories = action.payload;
            return {...state, categories: _categories};


        case mainstoreTypes.ADD_ORDERS_ITEM:
            // console.log(action.payload)
            var _arr = [...state.orders];
            _arr.push(action.payload)
                
            return {...state, orders : _arr};

        case mainstoreTypes.REMOVE_ORDERS_ITEM:

            var _arr = [...state.orders];
            var _itemIndex = _arr.findIndex(e=>e.id == action.payload);
            _arr.splice(_itemIndex, 1)

            return {...state, orders : _arr};

        case mainstoreTypes.UPDATE_ORDERS_ITEM:
            var items = [...state.orders];

            var _itemIndex = items.findIndex(e=>e.id == action.payload.id);
            
            items[_itemIndex] = action.payload;
            
            return {...state, orders: items};



        default:
            return state;
    }
    
}