import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { Tutorial } from './mainstore.model'
import * as mainstoreTypes from './mainstore.types'


export class InitState implements Action {
    readonly type = mainstoreTypes.INIT_STATE

    constructor() {}
}

export class SetState implements Action {
    readonly type = mainstoreTypes.SET_STATE

    constructor(public payload: any = null) {}
}

export class SetHeroes implements Action {
    readonly type = mainstoreTypes.SET_HEROES

    constructor(public payload: any[]) {}
}

export class SetCategories implements Action {
    readonly type = mainstoreTypes.SET_CATEGORIES

    constructor(public payload: any[]) {}
}

export class SetFeatured implements Action {
    readonly type = mainstoreTypes.SET_FEATURED

    constructor(public payload: any[]) {}
}

export class SetMostViewed implements Action {
    readonly type = mainstoreTypes.SET_MOST_VIEWED

    constructor(public payload: any[]) {}
}

export class SetUser implements Action {
    readonly type = mainstoreTypes.SET_USER

    constructor(public payload: Object) {}
}

export class UpdateUser implements Action {
    readonly type = mainstoreTypes.UPDATE_USER

    constructor(public payload: Object) {}
}


export class SetBasket implements Action {
    readonly type = mainstoreTypes.SET_BASKET

    constructor(public payload: any[]) {}
}
 
export class AddBasketItem implements Action {
    readonly type = mainstoreTypes.ADD_BASKET_ITEM

    constructor(public payload: any) {}
}

export class RemoveBasketItem implements Action {
    readonly type = mainstoreTypes.REMOVE_BASKET_ITEM

    constructor(public payload: number) {}
}

export class UpdateBasketItem implements Action {
    readonly type = mainstoreTypes.UPDATE_BASKET_ITEM

    constructor(public payload: any) {}
}




export class SetOrders implements Action {
    readonly type = mainstoreTypes.SET_ORDERS

    constructor(public payload: any[]) {}
}

export class AddOrdersItem implements Action {
    readonly type = mainstoreTypes.ADD_ORDERS_ITEM

    constructor(public payload: any) {}
}

export class RemoveOrdersItem implements Action {
    readonly type = mainstoreTypes.REMOVE_ORDERS_ITEM

    constructor(public payload: number) {}
}

export class UpdateOrdersItem implements Action {
    readonly type = mainstoreTypes.UPDATE_ORDERS_ITEM

    constructor(public payload: any) {}
}



export type Actions = 
InitState| 
SetState| 
SetHeroes|
SetCategories|
SetFeatured|
SetMostViewed|

SetUser|
UpdateUser|
SetBasket|
AddBasketItem|
RemoveBasketItem|
UpdateBasketItem|
SetOrders|
AddOrdersItem|
RemoveOrdersItem|
UpdateOrdersItem

;
