export const INIT_STATE         = 'INIT_STATE'
export const SET_STATE         = 'SET_STATE'
export const ADD_TUTORIAL       = '[TUTORIAL] Add'
export const REMOVE_TUTORIAL    = '[TUTORIAL] Remove'
export const SET_USER           = 'SET_USER'
export const UPDATE_USER        = 'UPDATE_USER'

export const SET_HEROES        = 'SET_HEROES'
export const SET_CATEGORIES        = 'SET_CATEGORIES'
export const SET_FEATURED        = 'SET_FEATURED'
export const SET_MOST_VIEWED        = 'SET_MOST_VIEWED'


export const SET_BASKET        = 'SET_BASKET'
export const ADD_BASKET_ITEM        = 'ADD_BASKET_ITEM'
export const REMOVE_BASKET_ITEM        = 'REMOVE_BASKET_ITEM'
export const UPDATE_BASKET_ITEM        = 'UPDATE_BASKET_ITEM'


export const SET_ORDERS        = 'SET_ORDERS'
export const ADD_ORDERS_ITEM        = 'ADD_ORDERS_ITEM'
export const REMOVE_ORDERS_ITEM        = 'REMOVE_ORDERS_ITEM'
export const UPDATE_ORDERS_ITEM        = 'UPDATE_BASKET_ITEM'

