import { api } from "./api.config";

const authService = {
  login,
  signup,
  findUser,
  validateAccount,
  logout,
 
};

const resource = "";


/**
 * signup method
 *
 * @param {{  username: string,email: string, password: string }} payload
 * @returns
 */
function signup(payload) {
  return api.postData(resource + "/auth/local/register", payload);
}

/**
 * signin method
 *
 * @param {{ identifier: string, password: string }} payload
 * @returns
 */
 function login(payload) {
  return api.postData(resource + "/auth/local", payload);
}


/**
 * FindOne method
 *
 * @param {} payload
 * @returns
 */
 function findUser(payload) {
  let filter = `?filters[email][$eq]=${payload.email}&[activationCode][$eq]=${payload.activationCode}`;
  return api.getData(resource + "/users/"+filter);
}

/**
 * validate account method
 *
 * @param {{id: number, email: string, activationCode: string }} payload
 * @returns
 */
 function validateAccount(payload) {
  // let filter = `?filter=[email][${payload.email}]&[activationCode][${payload.activationCode}]`;
  return api.putData(resource + "/users/"+payload.id, {activated : true}, {}, {});
}


/**
 * logout method
 *
 * @returns
 */
function logout() {
  return api.postData(resource + "/logout");
}
 

export default authService;
